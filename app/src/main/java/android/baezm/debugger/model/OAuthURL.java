package android.baezm.debugger.model;

import java.util.Random;

public class OAuthURL {

    public static String buildUrlWithParameters(String url, OAuthParameters oauthParameters) {
        String[] parameterNames = oauthParameters.getAllParameterNames();

        String fullUrl = url + "?";
        for(String name : parameterNames) {
            fullUrl += name + "=" + oauthParameters.getValueFromParameter(name) + "&";
        }

        return fullUrl;
    }

    public static String generateUniqueState(int sizeOfString) {
        String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        String randomString = "";

        Random random = new Random();
        for(int index = 0; index < sizeOfString; index++) {
            randomString += Character.toString(characters.charAt(random.nextInt(characters.length())));
        }

        return randomString;
    }
}
