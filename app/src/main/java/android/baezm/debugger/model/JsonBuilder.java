package android.baezm.debugger.model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JsonBuilder {

    public static JSONObject jsonObjectFromInputStream(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();
        String line;
        JSONObject jsonObject = null;

        try {
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            jsonObject = jsonObjectFromString(builder.toString());
        }
        catch (IOException error) {
            Log.e("JsonBuilder", "IOException: " + error);
        }

        return jsonObject;
    }

    public static JSONObject jsonObjectFromString(String jsonString) {
        JSONTokener tokener = new JSONTokener(jsonString);
        JSONObject jsonObject = null;
        try {
             jsonObject = new JSONObject(tokener);
        }
        catch (JSONException error) {
            Log.e("JsonBuilder", "IOException: " + error);
        }
        return jsonObject;
    }
}
