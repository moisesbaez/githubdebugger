package android.baezm.debugger.view;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.baezm.debugger.GitHubConnection;
import android.baezm.debugger.InstagramConnection;
import android.baezm.debugger.R;
import android.os.Bundle;

public class DebuggerActivity extends OAuthActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        oauthConnection = new GitHubConnection();
    }

    @Override
    protected Fragment createFragment() {
        return new AuthorizeFragment();
    }


    @Override
    public void setLayoutView() {
        replaceCurrentFragment(new ProfileFragment(), false);
    }

    @Override
    public void replaceCurrentFragment(Fragment newFragment, boolean addToStack) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.fragmentContainer, newFragment);
        if(addToStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }
}
