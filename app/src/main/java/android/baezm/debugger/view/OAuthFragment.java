package android.baezm.debugger.view;

import android.app.Activity;
import android.app.Fragment;
import android.baezm.debugger.model.OAuthConnection;
import android.baezm.debugger.model.OAuthParameters;

import org.json.JSONObject;

public abstract class OAuthFragment extends Fragment implements ApiDataView {
    public OAuthActivity oauthActivity;
    protected OAuthParameters oauthParameters;
    protected JSONObject jsonObject;
    protected String loadUrl;
    protected String httpRequestMethod;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        oauthActivity = (OAuthActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        oauthActivity = null;
    }

    @Override
    public String getUrlForApiCall() {
        return loadUrl;
    }

    @Override
    public OAuthConnection getOAuthConnection() {
        return oauthActivity.oauthConnection;
    }

    @Override
    public OAuthParameters getOAuthParameters() {
        return oauthParameters;
    }

    public String getHttpRequestMethod() {
        return httpRequestMethod;
    }

    @Override
    public abstract void onTaskFinished(String responseString);
}
