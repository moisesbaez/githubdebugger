package android.baezm.debugger.view;

import android.baezm.debugger.R;
import android.baezm.debugger.controller.GetRequestTask;
import android.baezm.debugger.model.JsonBuilder;
import android.baezm.debugger.model.OAuthParameters;
import android.baezm.debugger.model.OAuthURL;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;

public class ProfileFragment extends OAuthFragment {
    private TextView profileName;
    private TextView profileUsername;
    private TextView profileLocation;
    private Button editProfile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        oauthParameters = new OAuthParameters();
        oauthParameters.addParameter("access_token", getOAuthConnection().accessToken);

        loadUrl = OAuthURL.buildUrlWithParameters(getOAuthConnection().getApiUrl() + "/user", oauthParameters);
        new GetRequestTask().execute(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_profile, container, false);

        profileName = (TextView)fragmentView.findViewById(R.id.profileName);
        profileUsername = (TextView)fragmentView.findViewById(R.id.profileUsername);
        profileLocation = (TextView)fragmentView.findViewById(R.id.profileLocation);
        editProfile = (Button)fragmentView.findViewById(R.id.profileEdit);

        try {
            if (jsonObject != null) {
                profileName.setText(jsonObject.getString("name"));
                profileUsername.setText(jsonObject.getString("login"));
                profileLocation.setText(jsonObject.getString("location"));
            }
        }
        catch(JSONException error) {
            Log.e("Profile Fragment", "JSONException: " + error);
        }

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oauthActivity.replaceCurrentFragment(new EditProfileFragment(), true);
            }
        });

        return fragmentView;
    }

    @Override
    public void onTaskFinished(String responseString) {
        jsonObject = JsonBuilder.jsonObjectFromString(responseString);

        try {
            profileName.setText(jsonObject.getString("name"));
            profileUsername.setText(jsonObject.getString("login"));
            profileLocation.setText(jsonObject.getString("location"));
        }
        catch (JSONException error) {
            Log.e("ProfileFragment", "Profile: " + error);
        }
    }
}
