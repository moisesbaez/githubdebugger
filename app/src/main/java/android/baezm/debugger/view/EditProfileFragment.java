package android.baezm.debugger.view;

import android.baezm.debugger.R;
import android.baezm.debugger.controller.PostRequestTask;
import android.baezm.debugger.model.OAuthParameters;
import android.baezm.debugger.model.OAuthURL;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class EditProfileFragment extends OAuthFragment {
    private EditText editLocation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        oauthParameters = new OAuthParameters();
        oauthParameters.addParameter("access_token", getOAuthConnection().accessToken);
        loadUrl = OAuthURL.buildUrlWithParameters(getOAuthConnection().getApiUrl() + "/user", oauthParameters);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_profile_edit, container, false);

        editLocation = (EditText)fragmentView.findViewById(R.id.profileEditLocation);
        Button submitEdits = (Button)fragmentView.findViewById(R.id.profileSubmitEdits);

        submitEdits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oauthParameters = new OAuthParameters();
                oauthParameters.addParameter("location", editLocation.getText().toString());

                httpRequestMethod = "PATCH";
                new PostRequestTask().execute(EditProfileFragment.this);
            }
        });

        return fragmentView;
    }

    @Override
    public void onTaskFinished(String responseString) {
        oauthActivity.replaceCurrentFragment(new ProfileFragment(), true);
    }
}
