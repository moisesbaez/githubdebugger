package android.baezm.debugger.view;

import android.baezm.debugger.R;
import android.baezm.debugger.controller.AuthorizeWebClient;
import android.baezm.debugger.model.JsonBuilder;
import android.baezm.debugger.model.OAuthParameters;
import android.baezm.debugger.model.OAuthURL;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import org.json.JSONException;

public class AuthorizeFragment extends OAuthFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        oauthParameters = new OAuthParameters();
        oauthParameters.addParameter("client_id", getOAuthConnection().getClientID());
        oauthParameters.addParameter("redirect_uri", getOAuthConnection().getRedirectUrl());
        oauthParameters.addParameter("state", OAuthURL.generateUniqueState(16));
        oauthParameters.addParameter("response_type", "code");
        oauthParameters.addParameter("scope", "user");
        getOAuthConnection().state = oauthParameters.getValueFromParameter("state");

        loadUrl = OAuthURL.buildUrlWithParameters(getOAuthConnection().getAuthorizeUrl(), oauthParameters);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_authorize, container, false);
        WebView authorizeWebView = (WebView)fragmentView.findViewById(R.id.authorizeWebView);
        authorizeWebView.setWebViewClient(new AuthorizeWebClient(this));

        authorizeWebView.loadUrl(loadUrl);
        return fragmentView;
    }

    @Override
    public void onTaskFinished(String responseString) {
        jsonObject = JsonBuilder.jsonObjectFromString(responseString);

        try {
            getOAuthConnection().accessToken = jsonObject.getString("access_token");
        }
        catch (JSONException error) {
            Log.e("AuthorizeFragment", "JSONException: " + error);
        }

        oauthActivity.setLayoutView();
    }
}
