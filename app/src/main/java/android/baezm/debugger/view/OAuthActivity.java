package android.baezm.debugger.view;

import android.app.Fragment;
import android.baezm.debugger.model.OAuthConnection;

public abstract class OAuthActivity extends FragmentActivity {
    public OAuthConnection oauthConnection;

    public abstract void setLayoutView();
    public abstract void replaceCurrentFragment(Fragment newFragment, boolean addToStack);
}
