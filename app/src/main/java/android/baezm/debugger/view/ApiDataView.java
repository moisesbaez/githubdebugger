package android.baezm.debugger.view;

import android.baezm.debugger.model.OAuthConnection;
import android.baezm.debugger.model.OAuthParameters;

public interface ApiDataView {
    String getUrlForApiCall();
    OAuthConnection getOAuthConnection();
    OAuthParameters getOAuthParameters();
    String getHttpRequestMethod();
    void onTaskFinished(String responseString);
}
