package android.baezm.debugger.controller;

import android.baezm.debugger.model.OAuthConnection;
import android.baezm.debugger.view.ApiDataView;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AuthorizeWebClient extends WebViewClient {
    private ApiDataView apiDataView;
    private OAuthConnection oauthConnection;

    public AuthorizeWebClient(ApiDataView apiDataView) {
        this.apiDataView = apiDataView;
        oauthConnection = this.apiDataView.getOAuthConnection();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Uri loadingUri = Uri.parse(url);
        String loadingHost = loadingUri.getHost();

        Uri redirectUri = Uri.parse(oauthConnection.getRedirectUrl());
        String redirectHost = redirectUri.getHost();

        if(loadingHost.equals(redirectHost)) {
            String state = loadingUri.getQueryParameter("state");
            if(state.equals(oauthConnection.state)) {
                oauthConnection.code = loadingUri.getQueryParameter("code");
                AccessTokenTask tokenTask = new AccessTokenTask();
                tokenTask.execute(apiDataView);
                return true;
            }
        }

        return super.shouldOverrideUrlLoading(view, url);
    }
}
